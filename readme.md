### gitlab + jenkin setup
https://news.cloud365.vn/ci-cd-phan-3-huong-dan-tich-hop-jenkins-va-gitlab/

### jerkin workspace
/var/lib/jenkins/workspace

### script
```
pipeline {
    agent any
    
    stages {
        stage('Build') {
            steps {
                // Get some code from a GitHub repository
                git 'https://gitlab.com/giacatluong20121996/gitlab-jenkin-ci.git'

                // Run Maven on a Unix agent.
                sh 'docker-compose up -d’
            }
        }
    }
}

```

### issues: 
1. Docker-compose not found:  https://stackoverflow.com/questions/52052347/docker-compose-command-not-found-jenkins-pipeline
2. Jenkins cannot connect to docker daemon : https://stackoverflow.com/questions/38105308/jenkins-cant-connect-to-docker-daemon